# Python

Official site: https://www.python.org/

Docs: https://docs.python.org/3/

Tutorial: https://docs.python.org/3/tutorial/index.html

## 1. Installation

Python is cross-platform. You can use it on Windows, MacOS, Linux, etc. Please install latest version (3.10) from here (assuming you have Windows x64):

https://www.python.org/ftp/python/3.10.1/python-3.10.1-amd64.exe

During installation, set checkbox "Add Python 3.10 to PATH". It'll allow you to run Python from command line just by typing `py.exe`.

In some tutorials you are called to install "supercharged" version of python called Anakonda. I do not recomend you to do it :)

## 2. First test

There are 3 ways to run Python code:

**I. REPL (interactive mode)** - Usually people use this mode only for experiments or as a calculator.

To test this mode, open any command line you want (cmd.exe or PowerShell) and run
 `py.exe`.

You will see interactive python console like this:
```
Python 3.10.1 (tags/v3.10.1:2cd268a, Dec  6 2021, 19:10:37) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Then you can start to compute something. Press Enter after each line of code and this line will be immediately executed:

Get some help:

```python 
help()
```

Add 2 numbers:

```python 
4 + 5.5
 ```

More complex formula:

```python 
(1.5 * (400 + 150)) / 43
 ```

Display list of even numbers from 0 to 800:

```python 
[num for num in range(801) if num % 2 == 0]
```

... of odd numbers:

```python 
[num for num in range(801) if num % 2 != 0]
```

Exit from REPL

```python 
exit()
```

**II. Pass python program file (aka "module") to Python interpreter** - Most popular method, especially in production

To test this mode:

1. Create empty `.py` file (module) anywhere. For example, let's name it `word_stats.py`

2. Open the file and write some code to file with any raw text editor you want: Notepad, Notepad++, IDLE (ships with Python):

```python
import os
from collections import Counter

text = """
He missed Hogwarts so much it was like having a constant stomachache. He missed the castle, with its secret passageways and ghosts, his classes (though perhaps not Snape, the Potions master), the mail arriving by owl, eating banquets in the Great Hall, sleeping in his four-poster bed in the tower dormitory, visiting the gamekeeper, Hagrid, in his cabin next to the Forbidden Forest in the grounds, and, especially, Quidditch, the most popular sport in the wizarding world (six tall goal posts, four flying balls, and fourteen players on broomsticks).
"""

# Let's count words

word_count = len(text.strip().split(' '))

print(f'Total words: {word_count}')


# Let's count letters

text = text.lower()

letter_count_map = Counter([letter for letter in text if 'a' <= letter <= 'z'])

print('---------------------')

print(f'Letter count:')

for letter in sorted(letter_count_map.keys()):
    print(f'{letter}: {letter_count_map[letter]}')


# Display "press any key to continue" and wait

os.system('pause')

```

3. Use any command line tool you want (cmd.exe, PowerShell) to launch the program passing it to input of Python interpreter (assuming, program located at `C:\Users\vasiliy\Desktop\word_count.py`):

```
py.exe C:\Users\vasiliy\Desktop\word_count.py
```

4. See the result


**II. Double click on python program file (word_count.py) or right-click and select (Open with -> Python)**

If Python installed properly, It should work


## 3. Recomended learning plan

1. Complete steps above;

2. Explore fundamentals (tutorial here: https://docs.python.org/3/tutorial/index.html). I recomend to learn:

    - An introduction - https://docs.python.org/3/tutorial/introduction.html
    - Standard data types (`bool`, `int`, `float`, `str`, ...) - https://docs.python.org/3/library/stdtypes.html
    - Control flow - https://docs.python.org/3/tutorial/controlflow.html
    - Data structures - https://docs.python.org/3/tutorial/datastructures.html
    - Functions - https://docs.python.org/3/tutorial/controlflow.html#defining-functions, https://docs.python.org/3/tutorial/controlflow.html#more-on-defining-functions

3. Start course (one of courses below). 

    - https://ru.coursera.org/specializations/python

    - https://ru.coursera.org/specializations/python-3-programming

    - https://www.udemy.com/course/complete-python-bootcamp

    - https://learn.python.ru/


In addition, you can contact me and I'll answer your questions and prepare you examples and exercises from "real world" :)
